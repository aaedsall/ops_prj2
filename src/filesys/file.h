#ifndef FILESYS_FILE_H
#define FILESYS_FILE_H

#include "filesys/off_t.h"

struct inode;

/* Opening and closing files. */
struct file *file_open (struct inode *);
struct file *file_reopen (struct file *);
void file_close (struct file *);
struct inode *file_get_inode (struct file *);

/* Reading and writing. */
off_t file_read (struct file *, void *, off_t);  //off_t size bytes into buffer and increments file pointer to fp+size
off_t file_read_at (struct file *, void *, off_t size, off_t start);  //reads size bytes from the specified point start
off_t file_write (struct file *, const void *, off_t);
off_t file_write_at (struct file *, const void *, off_t size, off_t start);

/* Preventing writes. */
void file_deny_write (struct file *);
void file_allow_write (struct file *);

/* File position. */
void file_seek (struct file *, off_t);   // sets the file pointer to the off_T
off_t file_tell (struct file *);	//returns the current file pointer position
off_t file_length (struct file *); 	//returns the size of the file

#endif /* filesys/file.h */
