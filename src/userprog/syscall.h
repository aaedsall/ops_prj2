#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include <stdbool.h>
#include <debug.h>



void syscall_init (void);

void halt_ (void) NO_RETURN;
void exit_ (int status) NO_RETURN;
int exec_ (const char *file);
int wait_ (int pid);
bool create_ (const char *file, unsigned initial_size);
bool remove_ (const char *file);
int open_ (const char *file);
int filesize_ (int fd);
int read_ (int fd, void *buffer, unsigned length);
int write_ (int fd, const void *buffer, unsigned length);
void seek_ (int fd, unsigned position);
unsigned tell_ (int fd);
void close_ (int fd);



#endif /* userprog/syscall.h */
