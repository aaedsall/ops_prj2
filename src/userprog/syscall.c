#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include <debug.h>
#include "threads/thread.h"
#include "devices/shutdown.h"
#include "filesys/filesys.h"
#include "filesys/directory.h"
#include "userprog/process.h"
#include "filesys/file.h"
#include "devices/intq.h"	// for input_getc()
#include "threads/vaddr.h"
#include "filesys/inode.h"
#include "userprog/pagedir.h"
#include "threads/malloc.h"

#define ONE 1
#define TWO 2
#define THREE 3
#define ERR -1
#define ZERO 0
#define FOUR 4

static void syscall_handler (struct intr_frame *);
struct lock exec_lock;

void
syscall_init (void) 
{
printf("in syscall init\n");
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
syscall_handler (struct intr_frame *f) 
{
	/** May have to turn interrupts off for these? **/
	uint32_t * sys_num, arg_1, arg_2, arg_3;
	 
	
	if(!is_user_vaddr((const void * )f->esp) || f->esp == NULL
	 ||pagedir_get_page(thread_current()->pagedir,f->esp)==NULL  ){
		//printf("pg \n");
		exit_(-1);
	}
	
	//intr_dump_frame(f);
	// int return_num = ZERO; // Assuming -1 gives an error ?
	/** Terminates pintos. See devices/shutdown.h  **/
  
     sys_num = f->esp;
    
	arg_1 = *(sys_num+ONE);
	arg_2 = *(sys_num+TWO);
	arg_3 = *(sys_num+THREE);
	
	//debug_backtrace_all();
	switch (*sys_num) {
		case (SYS_HALT):
			shutdown_power_off();
			break;
		case (SYS_EXIT):			
			f->eax = (arg_1);
			exit_ (arg_1);
			break;
		case (SYS_EXEC):
			//hex_dump((uintptr_t)(f->esp), (f->esp), PHYS_BASE - (f->esp), true);
			
			f->eax = exec_((const char *)arg_1);
			break;
		case (SYS_WAIT):
			f->eax = wait_ ((int)arg_1);
			break;
		case (SYS_CREATE):
			f->eax = create_((const char *)arg_1, (unsigned)arg_2);
			break;
		case (SYS_REMOVE):
			f->eax = remove_((const char *)arg_1);
			break;
		case (SYS_OPEN):
			
			f->eax = open_((const char*)arg_1);
			break;
		case (SYS_FILESIZE):
			f->eax = filesize_((int)arg_1);
			break;
		case (SYS_READ):
			f->eax = read_((int) arg_1, (void *)arg_2, (unsigned) arg_3);
			break;
		case (SYS_WRITE):
		     //hex_dump((uintptr_t)(f->esp), (f->esp), PHYS_BASE - (f->esp), true);
			f->eax = write_((int)arg_1, (const void *)(arg_2),(unsigned) arg_3);
			break;
		case (SYS_SEEK):
			seek_((int) arg_1, (unsigned) arg_2);
			break;
		case (SYS_TELL):
			f->eax = tell_((int)arg_1);
			break;
		case (SYS_CLOSE):
			close_((int)arg_1);
			break;
		default:
			printf("System call %d unknown\n", sys_num);
	}

  //printf("calling thread exit in syscall handler \n");
  //thread_exit ();
}

int write_ (int fd, const void *  buffer, unsigned length){
  struct thread * current = thread_current();
	//printf("in write \n");
  int retno;
  if (length == 0) return ZERO;
  if (fd ==1){  
	  lock_acquire(&current->write_lock);
      putbuf((const char *)buffer,length);
      lock_release(&current->write_lock);
	  return 0;
  }
  else{
	int i=0;
	for ( i=2; i<127; i++){
		if (i == fd) {
			//lock_acquire(&current->write_lock);
			//retno = file_write(&current->fds[i], buffer, length);
			// Confused on this -- think maybe error in open
			//printf("calling filewrie %s\n",(const char *)buffer);
			return file_write(fd, buffer, length);
			//lock_release(&current->write_lock);
			}	
		}
		// Not getting here ??
	}
	return ERR;
}

// void halt (void) NO_RETURN;

void exit_ (int status) {
		if(status <-1)
			exit_(-1);
			
		thread_current()->exit_status = status;
		
		sema_up(&thread_current()->exec_sem);
		/* Calls process exit */
		printf ("%s: exit(%d)\n", thread_current()->name,status);	//compulsory print the full name passed to process exec
		thread_exit();	
}

int exec_ (const char *file){
	// ASSERT file != NULL;
	tid_t new_thread;
	struct lock _wait;
   struct condition _cond;
	struct thread * current = thread_current();
	
	sema_down(&current->exec_sem);
	new_thread = process_execute (file);
	current->child_uhh=child_thread(new_thread);
	child_thread(new_thread)->Parent = true;
	if (new_thread == TID_ERROR){
		printf("Thread cannot be created in process_execute()\n");
		sema_up(&current->exec_sem);
		return ERR;
	}
    sema_up(&current->exec_sem);
  
 
	return new_thread;
}

int wait_ (int pid) {
	// TODO: Ensure that no parent waits on the same child twice
	int kid_pid;
	//if (thread_current()->wait) return ERR;
	kid_pid = process_wait(pid);
	//thread_current()->wait = true;
	return kid_pid;
}

bool create_ (const char * file, unsigned initial_size){
	bool retno;
	if (file == NULL){
		 // printf("Error in bool create file\n");
		 exit_(-1);
	}
	else if (file =="") return false;
	retno = filesys_create(file, initial_size);
	if (!retno){
		//printf("Error creating file\n");
		return false;
	}
	return true;
}

bool remove_ (const char *file){
	bool retno;
	if (file == NULL){
		// or exit idk
		return false;
	}
	retno = filesys_remove(file);
	return retno;
}


/* Returns fd or -1 if cannot open */
int open_ (const char* file) {
	
	if (file == NULL) return -1;
	struct file * fd;
	int ret_fd;
	//int fd_new= filesys_open(file);
	struct thread * current = thread_current();
	struct dir * thisdir;
	struct inode * tempnode;
	thisdir = dir_open_root();
	if (thisdir == NULL) return ERR;

	// bool samefilename = dir_lookup (thisdir, file, &tempnode);
	//if (samefilename) printf("here %s\n", file);

	fd = filesys_open(file);
	if (fd == NULL || fd ==-1) {
		return -1;
	}
	int i=0;
	for ( i = 2; i < 126; i++){
		if (current->fds[i] == 0){
			//current->fds[i] = (struct file *)malloc(sizeof *file);
			// Print to check concurrent fds in threads
			//printf("open size %d\n");
			//printf("threadid(open): %d\n", current->tid);
			current->fds[i] = fd;
			file_deny_write(current->fds[i]);
			return i;
		}
	}
	return ret_fd;
	
	
	return ret_fd;
}
int filesize_ (int fd){
	struct thread * current = thread_current();
	int i=0;
	for (i=2; i<126; i++){
		if (i == fd){
			if(current->fds[i] == NULL)
				printf("the file is null \n");
			return file_length(current->fds[i]);
		}
	}
	return ERR;
}

int read_ (int fd, void *buffer, unsigned length){
  if(fd == 0)
    return input_getc();
  else if (length == 0) return ZERO;
  else {
	
	
	struct thread * current = thread_current();
	int i=0;
	for ( i=2; i<126; i++){
		if (i == fd) {
			return file_read(current->fds[i], buffer, length);
		}
	}
    return ERR;
}

}
	
		
		
void seek_ (int fd, unsigned position){
  struct thread * current = thread_current();
  int i=0;
	for ( i=2; i<126; i++){
		if (i == fd) {
			file_seek(current->fds[i],position);
		}
	}
}

unsigned tell_ (int fd){
  struct thread * current = thread_current();
	int i=0;
	for ( i=2; i<126; i++){
		if (i == fd) {
			return file_tell(current->fds[i]);
		}
	}
return ERR;
}


void close_ (int fd){
  struct thread * current = thread_current();
	int i=0;
	for ( i=2; i<126; i++){
		if (i == fd) {
			current->fds[fd] = ZERO;			
			file_close(current->fds[i]);			
		}
	}
}


